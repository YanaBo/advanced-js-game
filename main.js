class squareGame {
  static getRandomInRange = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

  constructor() {
    this.row = 10;
    this.columns = 10;
    this.arr = [];
    this.randomSquareInx = null;
    this.$table = $('#table');
    this.$levelBtn = $('.button');
    this.$counterPC = $('#pc');
    this.$counterHuman = $('#human');
    this.timer = this.setTimer(1500);
    this.counterPC = 0;
    this.counterHuman = 0;
    this.win = 50;
    this.init();
  }

  getSpeed = (level) => {
    switch (level) {
      case 'Light':
        return 1500;

      case 'Medium':
        return 1000;

      case 'Hard':
        return 500;

      default:
        return 'Light';
    }
  };

  createTable = () => {
    let k = 0;
    for (let i = 0; i < this.arr.length; i++) {
      let tr = document.createElement('tr');

      for (let j = 0; j < this.arr[i].length; j++) {
        let td = document.createElement('td');
        td.setAttribute('data-id', k);
        k++;
        tr.appendChild(td);
      }

      this.$table[0].appendChild(tr);
    }
  };

  createSquares = () => {
    let k = 0;
    for (let i = 0; i < this.row; i++) {
      this.arr[i] = [];

      for (let j = 0; j < this.columns; j++) {
        this.arr[i][j] = k;
        k++;
      }
    }
  };

  handleClickSquare = () => {
    this.$table.on('click', (event) => {
      const $el = $(event.target);
      const id = $el.data('id');
      const randomSquareInx = this.randomSquareInx;
      const $tds = this.$table.find('td');

      if (!id) {
        event.preventDefault();
        return;
      }

      if (id === randomSquareInx) {
        $($tds[this.randomSquareInx]).addClass('success');
      } else {
        $($tds[this.randomSquareInx]).addClass('fail');
      }
    })
  };

  handleClickLevel = () => {
    this.$levelBtn.on('click', (event) => {
      clearInterval(this.timer);
      const $btn = $(event.target);
      const btnValue = $btn.text();
      const speed = this.getSpeed(btnValue);
      this.timer = this.setTimer(speed);
    })
  };

  setTimer = time => setInterval(() => {
    this.randomSquareInx = squareGame.getRandomInRange(0, 99);
    let counterPC = this.counterPC;
    const $tds = this.$table.find('td');
    const $activeSquare = $($tds[this.randomSquareInx]);
    let result;
    $tds.removeClass('blue');
    $activeSquare.addClass('blue');

    setTimeout(() => {
      if (!$activeSquare.hasClass('success')){
        $activeSquare.addClass('fail');
      }

      this.counterPC = this.$table.find('.fail').length;
      this.counterHuman = this.$table.find('.success').length;

      this.$counterPC.text(this.counterPC);
      this.$counterHuman.text(this.counterHuman);

      if (this.counterPC === this.win) {
        result = confirm('PC Wins! Do you want to replay the game?');
      } else if(this.counterHuman === this.win) {
        result = confirm('Human Wins! Do you want to replay the game?');
      }

      if (typeof result === "boolean") {
        window.location.reload();
      }
    }, time);

  }, time);

  init = () => {
    this.createSquares();
    this.createTable();
    this.handleClickSquare();
    this.handleClickLevel();
  }
}

new squareGame();
